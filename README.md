# Rasperry Pi CI/CD

CI/CD example project for Raspberry Pi

## Installation
1. Install docker on Raspberry Pi using the [convenience script](https://docs.docker.com/install/linux/docker-ce/debian/#install-using-the-convenience-script).
2. Install gitlab-runner using the following command:<br />
```$ sudo apt install -y gitlab-runner```
3. Run this command to add gitlab-runner to docker group:<br />
```$ sudo usermod -aG docker gitlab-runner```
4. Comment out these three lines in ~/.bash_logout:<br />
```
#if [ "$SHLVL" = 1 ]; then
#    [ -x /usr/bin/clear_console ] && /usr/bin/clear_console -q
#fi
```
5. Configure the runner and start building!

## Tested Platforms
This has been tested on a Raspberry Pi 2 and Raspberry Pi 4