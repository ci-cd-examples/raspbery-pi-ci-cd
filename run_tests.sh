# Run a basic echo test
echo "This is a basic echo test."

# Test to see that BOSSA is working
echo "Calling BOSSA from shell script."
/BOSSA/bin/bossac --version

# Pull the example project from Github
git clone https://github.com/amahpour/SAM4E-XPlained-Pro-HIL-Example.git