#!/bin/sh
if [ $# -eq 0 ]; then
  echo "Usage: $0 <Gitlab-Registration-Token>" >&2
  exit 1
fi

# Install Docker
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh

# Install gitlab-runner (using "pi" user)
curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_armhf.deb && \
    dpkg -i gitlab-runner_armhf.deb
gitlab-runner install --user pi
usermod -aG docker pi

# Register Gitlab runner
gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token $1 \
  --executor docker \
  --docker-image debian \
  --description "Raspberry Pi Runner" \
  --tag-list "rpi,"
  
gitlab-runner start